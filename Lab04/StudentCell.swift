//
//  StudentCell.swift
//  Lab04
//
//  Created by Jacek on 15/03/2019.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class StudentCell: UITableViewCell {
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var indexNumber: UILabel!
    @IBOutlet weak var labInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
