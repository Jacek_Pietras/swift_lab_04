//
//  ViewController.swift
//  Lab04
//
//  Created by Jacek on 14/03/2019.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit
var data: [Student] = []
class ViewController: UIViewController {
   
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        data = [Student(firstName: "Homer", lastName: "Simson", indexNumber: "12345", lab: Lab(number: 1, points: 2.5)),
                Student(firstName: "Lisa", lastName: "Simson", indexNumber: "121212", lab: Lab(number: 1, points: 3.0)),
                Student(firstName: "Marge", lastName: "Simson", indexNumber: "54321", lab: Lab(number: 1, points: 5.0)),
                Student(firstName: "Bart", lastName: "Simson", indexNumber: "343434", lab: Lab(number: 1, points: 4.5))]
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath) as! StudentCell
        let student = data[indexPath.row]
        cell.firstName.text = student.firstName
        cell.lastName.text = student.lastName
        cell.indexNumber.text = student.indexNumber
        cell.labInfo.text = String(student.lab.points)

        return cell
    }
}

