//
//  DataModel.swift
//  Lab04
//
//  Created by Jacek on 14/03/2019.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
class Student {
    var firstName: String
    var lastName: String
    var indexNumber: String
    var lab: Lab
    init(firstName: String, lastName: String, indexNumber: String, lab: Lab) {
        self.firstName = firstName
        self.lastName = lastName
        self.indexNumber = indexNumber
        self.lab = lab
    }
}
struct Lab {
    var number: Int
    var points: Double
    init(number: Int, points: Double) {
        self.number = number
        self.points = points
    }
}
